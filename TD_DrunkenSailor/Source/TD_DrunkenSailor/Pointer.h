// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/DecalComponent.h"
#include "Components/SceneComponent.h"
#include "Pointer.generated.h"

UCLASS()
class TD_DRUNKENSAILOR_API APointer : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APointer();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditDefaultsOnly)
		USceneComponent* Root;
	UPROPERTY(EditDefaultsOnly)
		UDecalComponent* Decal;

	bool BSet=true;

};
