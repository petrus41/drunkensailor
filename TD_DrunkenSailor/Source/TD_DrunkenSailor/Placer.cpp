// Fill out your copyright notice in the Description page of Project Settings.


#include "Placer.h"

// Sets default values
APlacer::APlacer()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void APlacer::BeginPlay()
{
	Super::BeginPlay();
	OnClicked.AddUniqueDynamic(this, &APlacer::OnSelected);
	
}

// Called every frame
void APlacer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APlacer::OnSelected(AActor* TouchedActor, FKey ButtonPressed)
{
	Cliked.Broadcast();
}