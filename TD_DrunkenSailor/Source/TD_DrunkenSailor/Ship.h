// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Bullet.h"
#include "TD_DrunkenSailorGameModeBase.h"
#include "Components/CapsuleComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SceneComponent.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "GameFramework/Pawn.h"
#include "Ship.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FHit);


UCLASS()
class TD_DRUNKENSAILOR_API AShip : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AShip();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	AShip(const FObjectInitializer& OI);

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite)
		UFloatingPawnMovement* MovementComponent = nullptr;
	UPROPERTY(EditDefaultsOnly)
		UStaticMeshComponent* Mesh;
	UPROPERTY(EditDefaultsOnly)
		USceneComponent* Root;
	UPROPERTY(EditDefaultsOnly)
		UCapsuleComponent* Capsule;

	UPROPERTY(BlueprintCallable, BlueprintAssignable)
		FHit Hitted;
	UPROPERTY(BlueprintCallable, BlueprintAssignable)
		FHit Eliminated;
	UPROPERTY(BlueprintCallable, BlueprintAssignable)
		FHit VillageHitted;

	float rallentamento=0;
	ABullet* Hitter = nullptr;
	ABullet* NewBullet = nullptr;
	TArray<AActor*> Targets;
	ATD_DrunkenSailorGameModeBase* GameMode = nullptr;
	float Timer = 0;
	UPROPERTY(EditDefaultsOnly, Category = "ActorSpawning")
		TSubclassOf<ABullet> Bullet;
	UPROPERTY(BlueprintReadWrite)
		float MaxHP = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float HP = 0;
	UPROPERTY(EditAnywhere)
		float Speed = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Armor = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float FireRate = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Damage = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Area = 0;
	UPROPERTY(EditAnywhere)
		float VillageDamege = 0;
	UPROPERTY(EditAnywhere)
		float TravelTime = 0;
	UPROPERTY(EditAnywhere)
		bool BParable = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Award = 0;

	void Spawn();

	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void Overlapping(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void EndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	UFUNCTION(BlueprintCallable)
		void Colpita(float danni,float slow);
};
