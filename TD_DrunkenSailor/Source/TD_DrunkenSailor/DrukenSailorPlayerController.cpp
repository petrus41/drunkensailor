// Fill out your copyright notice in the Description page of Project Settings.


#include "DrukenSailorPlayerController.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Engine/World.h"

ADrukenSailorPlayerController::ADrukenSailorPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
	bEnableClickEvents = true;
}

void ADrukenSailorPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	//InputComponent->BindAxis("MoveUp", this, &ADrukenSailorPlayerController::Up);
	InputComponent->BindAction("Start", IE_Pressed, this, &ADrukenSailorPlayerController::Start);
	InputComponent->BindAction("Fire", IE_Pressed, this, &ADrukenSailorPlayerController::Fire);
	InputComponent->BindAction("Quit", IE_Pressed, this, &ADrukenSailorPlayerController::Exit);
	InputComponent->BindAxis("Rotation", this, &ADrukenSailorPlayerController::Rotate);
	InputComponent->BindAxis("MoveForward", this, &ADrukenSailorPlayerController::Forward);
	InputComponent->BindAxis("MoveLeft", this, &ADrukenSailorPlayerController::Left);
}

void ADrukenSailorPlayerController::BeginPlay()
{
	Super::BeginPlay();
	Pawn = Cast<APawnCamera>(GetPawn());
}

void ADrukenSailorPlayerController::Rotate(float Direction)
{
	if (!Pawn)
	{
		UE_LOG(LogTemp, Error, TEXT("Pawn"));
	}
	else
	{
		Pawn->AddActorWorldRotation(FRotator(0, (Pawn->RotationSpeed * GetWorld()->DeltaTimeSeconds * Direction), 0));
	}
}

void ADrukenSailorPlayerController::Forward(float Direction)
{
	if (!Pawn)
	{
		UE_LOG(LogTemp, Error, TEXT("Pawn"));
	}
	else
	{
		FVector NewPos= Pawn->GetActorLocation() + UKismetMathLibrary::GetForwardVector(FRotator(0, Pawn->GetActorRotation().Yaw, 0)) * Direction * Pawn->MovementSpeed * GetWorld()->DeltaTimeSeconds;
		if (NewPos.X >= Pawn->XYLimit.Xp)
		{
			NewPos.X = Pawn->XYLimit.Xp;
		}
		if (NewPos.X <= Pawn->XYLimit.Xn)
		{
			NewPos.X = Pawn->XYLimit.Xn;
		}
		if (NewPos.Y >= Pawn->XYLimit.Yp)
		{
			NewPos.Y = Pawn->XYLimit.Yp;
		}
		if (NewPos.Y <= Pawn->XYLimit.Yn)
		{
			NewPos.Y = Pawn->XYLimit.Yn;
		}
		Pawn->SetActorLocation(NewPos);
	}
}

void ADrukenSailorPlayerController::Left(float Direction)
{
	if (!Pawn)
	{
		UE_LOG(LogTemp, Error, TEXT("Pawn"));
	}
	else
	{
		FVector NewPos = Pawn->GetActorLocation() + UKismetMathLibrary::GetRightVector(FRotator(0, Pawn->GetActorRotation().Yaw, 0)) * Direction * Pawn->MovementSpeed * GetWorld()->DeltaTimeSeconds;
		if (NewPos.X >= Pawn->XYLimit.Xp)
		{
			NewPos.X = Pawn->XYLimit.Xp;
		}
		if (NewPos.X <= Pawn->XYLimit.Xn)
		{
			NewPos.X = Pawn->XYLimit.Xn;
		}
		if (NewPos.Y >= Pawn->XYLimit.Yp)
		{
			NewPos.Y = Pawn->XYLimit.Yp;
		}
		if (NewPos.Y <= Pawn->XYLimit.Yn)
		{
			NewPos.Y = Pawn->XYLimit.Yn;
		}
		Pawn->SetActorLocation(NewPos);
	}
}

void ADrukenSailorPlayerController::Up(float Direction)
{
	if (!Pawn)
	{
		UE_LOG(LogTemp, Error, TEXT("Pawn"));
	}
	else
	{
		Pawn->SetActorLocation(Pawn->GetActorLocation() + FVector(0, 0, 1) * Direction * Pawn->MovementSpeed * GetWorld()->DeltaTimeSeconds);
	}
}


void ADrukenSailorPlayerController::Start()
{
	Begin.Broadcast();
}

void ADrukenSailorPlayerController::Fire()
{
	Shoot.Broadcast();
}

void ADrukenSailorPlayerController::Exit()
{
	UKismetSystemLibrary::QuitGame(this, nullptr, EQuitPreference::Quit, false);
}