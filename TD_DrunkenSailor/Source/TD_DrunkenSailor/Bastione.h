// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Bullet.h"
#include "Pointer.h"
#include "DrukenSailorPlayerController.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SceneComponent.h"
#include "Bastione.generated.h"

UCLASS()
class TD_DRUNKENSAILOR_API ABastione : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABastione();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditDefaultsOnly)
		USceneComponent* Root;
	UPROPERTY(EditDefaultsOnly)
		UStaticMeshComponent* Bastione;

	APointer* NewPointer = nullptr;
	ABullet* NewBullet = nullptr;
	UPROPERTY(EditAnywhere)
		float CDTimer;
	UPROPERTY(EditAnywhere)
		float Damage;
	UPROPERTY(EditAnywhere)
		float TravelTime;
	UPROPERTY(EditAnywhere)
		bool BParable;
	UPROPERTY(EditAnywhere)
		float AOE;
	float Timer=0;
	bool BReady = true;
	bool BTimer = false;
	bool BArmed = true;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ABullet> Bullet;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<APointer> Pointer;
	UFUNCTION()
		void Set();
	UFUNCTION()
		void OnSelected(AActor* TouchedActor, FKey ButtonPressed);
};
