// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SceneComponent.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Bullet.generated.h"

UENUM(BlueprintType)
enum class EGeneretor:uint8
{
	Tower,
	Ship,
	AOE
};

UCLASS()
class TD_DRUNKENSAILOR_API ABullet : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABullet();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditDefaultsOnly)
		UStaticMeshComponent* Mesh;

	UPROPERTY(BlueprintReadWrite)
		AActor* Target = nullptr;
	UPROPERTY(BlueprintReadWrite)
		EGeneretor Generetor;
	UPROPERTY(BlueprintReadWrite)
		float Damage = 0;
	UPROPERTY(BlueprintReadWrite)
		float Slow = 1;
		float TravelTime = 6;
		float Timer = 0;
		bool BParable = false;
		float Gravity = -1000;
		float Speed = 1000;
		UPROPERTY(BlueprintReadWrite)
			float AOERange = 0;
		FVector Start;
		FVector End;

		UFUNCTION(BlueprintImplementableEvent)
			void AOECollision();
};
