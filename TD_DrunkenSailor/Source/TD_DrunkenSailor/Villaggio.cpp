// Fill out your copyright notice in the Description page of Project Settings.


#include "Villaggio.h"
#include "Ship.h"

// Sets default values
AVillaggio::AVillaggio()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Mesh= CreateDefaultSubobject<USceneComponent>(TEXT("Mesh"));
	SetRootComponent(Mesh);
}

// Called when the game starts or when spawned
void AVillaggio::BeginPlay()
{
	Super::BeginPlay();
	OnActorBeginOverlap.AddDynamic(this, &AVillaggio::OnOverlapBegin);
}

// Called every frame
void AVillaggio::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AVillaggio::OnOverlapBegin(AActor* OverlappedActor, AActor* OtherActor)
{
	AShip* Hitter=Cast<AShip>(OtherActor);
	if(Hitter)
	{
		HP -= Hitter->VillageDamege;
		End();
	}
}