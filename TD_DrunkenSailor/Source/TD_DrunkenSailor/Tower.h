// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Placer.h"
#include "Bullet.h"
#include "TD_DrunkenSailorGameModeBase.h"
#include "Components/CapsuleComponent.h"
#include "Components/SceneComponent.h"
#include "Components/DecalComponent.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/Actor.h"
#include "Tower.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FHitted);

USTRUCT(BlueprintType)
struct FStatistiche
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float MAXHP;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float FireRate;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Raggio;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Damage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Slow;
};

UCLASS()
class TD_DRUNKENSAILOR_API ATower : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATower();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:

	ATower(const FObjectInitializer& OI);
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(BlueprintCallable, BlueprintAssignable)
		FHitted Hitted;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FStatistiche> Statistiche;
	UPROPERTY(EditDefaultsOnly)
		USceneComponent* Root;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UDecalComponent* Decal;
	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite)
		UStaticMeshComponent* Tower;
	UPROPERTY(EditDefaultsOnly)
		UCapsuleComponent* Capsule;
	UPROPERTY(BlueprintReadWrite)
		APlacer* Position = nullptr;
	UPROPERTY(BlueprintReadWrite)
		bool BSet = true;
	UPROPERTY(EditDefaultsOnly, Category = "ActorSpawning")
		TSubclassOf<ABullet> Bullet;
	UPROPERTY(EditAnywhere)
		float TravelTime = 0;
	UPROPERTY(EditAnywhere)
		float AOE = 0;
	UPROPERTY(EditAnywhere)
		bool BParable = false;
	UPROPERTY(BlueprintReadOnly)
		float HealthPrecent = 0.f;
	UPROPERTY(BlueprintReadWrite)
		ATD_DrunkenSailorGameModeBase* GameMode = nullptr;
	UPROPERTY(EditAnywhere)
		EGeneretor Generetor;
	UPROPERTY(BlueprintReadWrite)
		int Livello = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<int> Costi_Upgrade;


	TArray<AActor*> Targets;
	ABullet* Hitter = nullptr;
	ABullet* NewBullet = nullptr;
	float HP = 0;
	float Timer = 0.f;
	float RepairTimer = 0.f;
	bool BRepairing = false;
	float MissingHealthPrecent = 0.f;

	void Fire();

	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void Overlapping(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void EndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	UFUNCTION()
		void OnSelected(AActor* TouchedActor, FKey ButtonPressed);
	UFUNCTION()
		void Place();
	UFUNCTION(BlueprintCallable)
		void Repair();
	UFUNCTION(BlueprintImplementableEvent)
		void UpdateWidget();
	UFUNCTION(BlueprintImplementableEvent)
		void UpdateCollision();
	UFUNCTION(BlueprintImplementableEvent)
		void Anim();
	UFUNCTION(BlueprintCallable)
		void UpGrade();
};
