// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Placer.h"
#include "GameFramework/GameModeBase.h"
#include "TD_DrunkenSailorGameModeBase.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct FCosti
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Cannone;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Slower;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int AOE;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Checchino;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Ship;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Mortatio;
};

UCLASS()
class TD_DRUNKENSAILOR_API ATD_DrunkenSailorGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
public:
	ATD_DrunkenSailorGameModeBase();
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FCosti Costi;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
		TArray<APlacer*> AllPlacer;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
		int Gold = 100;
	UFUNCTION(BlueprintImplementableEvent)
		void WidgetUpdate();
};
