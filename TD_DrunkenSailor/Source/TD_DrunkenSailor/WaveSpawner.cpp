// Fill out your copyright notice in the Description page of Project Settings.


#include "WaveSpawner.h"

// Sets default values
AWaveSpawner::AWaveSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AWaveSpawner::BeginPlay()
{
	Super::BeginPlay();
	GetWorldTimerManager().SetTimer(SpawnTimerHandle, this, &AWaveSpawner::Spawn, SpawnRate, true,TimeBeforeSpawn);
}

// Called every frame
void AWaveSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AWaveSpawner::Spawn()
{
	FTransform MyTransform(GetActorRotation(), GetActorLocation(), GetActorScale3D());
	NewShip = GetWorld()->SpawnActor<AShip>(Ship, MyTransform);
	//UE_LOG(LogTemp, Error, TEXT("Spawned"));
	if (NewShip)
	{
		NewShip->Eliminated.AddUniqueDynamic(this,&AWaveSpawner::Check);
		NewShip->VillageHitted.AddUniqueDynamic(this, &AWaveSpawner::Check);
		SpawnerIteretor++;
		if (SpawnerIteretor >= WavePopulation)
		{
			GetWorldTimerManager().ClearTimer(SpawnTimerHandle);
		}
		Blueprint();
	}
}

void AWaveSpawner::Check()
{
	DeathIteretor++;
	if (DeathIteretor >= WavePopulation)
	{
		End.Broadcast();
	}
}
