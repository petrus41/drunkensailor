// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/SceneComponent.h"
#include "GameFramework/Pawn.h"
#include "PawnCamera.generated.h"

USTRUCT(BlueprintType)
struct FLimiti
{
	GENERATED_BODY()
		UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Xp;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Xn;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Yp;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Yn;
};


UCLASS()
class TD_DRUNKENSAILOR_API APawnCamera : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APawnCamera();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	APawnCamera(const FObjectInitializer& OI);
	UPROPERTY(EditDefaultsOnly)
		UCameraComponent* Camera = nullptr;
	UPROPERTY(EditDefaultsOnly)
		USceneComponent* Scene = nullptr;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		USpringArmComponent* SpringArm = nullptr;
	UPROPERTY(EditAnywhere)
		float RotationSpeed = 0.f;
	UPROPERTY(EditAnywhere)
		float MovementSpeed = 0.f;
	UPROPERTY(EditAnywhere)
		FLimiti XYLimit;
 
};
