// Fill out your copyright notice in the Description page of Project Settings.


#include "Bastione.h"

// Sets default values
ABastione::ABastione()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SetRootComponent(Root);
	Bastione= CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Bastione->SetupAttachment(Root);
}

// Called when the game starts or when spawned
void ABastione::BeginPlay()
{
	Super::BeginPlay();
	Cast<ADrukenSailorPlayerController>(GetWorld()->GetFirstPlayerController())->Shoot.AddDynamic(this, &ABastione::Set);
	OnClicked.AddUniqueDynamic(this, &ABastione::OnSelected);
	
}

// Called every frame
void ABastione::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (BTimer)
	{
		Timer += DeltaTime;
		if (BReady && BArmed)
		{
			if (Timer >= 0.5)
			{
				BReady = false;
				Timer = 0;
				BTimer = false;
			}
		}
		else 
		{
			if (Timer >= TravelTime)
			{
				NewPointer->Destroy();
				BReady = true;
			}
			if (Timer >= CDTimer)
			{
				BArmed = true;
				Timer = 0;
				BTimer = false;
			}
		}
	}

}

void ABastione::Set()
{
	if (!BReady&&BArmed)
	{
		//UE_LOG(LogTemp, Error, TEXT("%s"), *NewPointer->GetName());
		NewPointer->BSet = false;
		FTransform MyTransform(FRotator(0, 0, 0), FVector(GetActorLocation().X, GetActorLocation().Y, GetActorLocation().Z), FVector(1, 1, 1));
		NewBullet = GetWorld()->SpawnActorDeferred<ABullet>(Bullet, MyTransform);
		NewBullet->Target = NewPointer;
		NewBullet->Damage = Damage;
		NewBullet->Generetor = EGeneretor::AOE;
		NewBullet->BParable = BParable;
		NewBullet->AOERange = AOE;
		NewBullet->TravelTime = TravelTime;
		NewBullet->Start = FVector(GetActorLocation().X, GetActorLocation().Y, GetActorLocation().Z);
		NewBullet->FinishSpawning(MyTransform);
		BArmed = false;
		BTimer = true;
	}
}

void ABastione::OnSelected(AActor* TouchedActor, FKey ButtonPressed)
{
	if (BReady && BArmed)
	{
		FTransform MyTransform(FRotator(0, 0, 0), FVector(GetActorLocation().X, GetActorLocation().Y, GetActorLocation().Z), FVector(1, 1, 1));
		NewPointer = GetWorld()->SpawnActor<APointer>(Pointer, MyTransform);
		BTimer = true;
	}
}