// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/PrimitiveComponent.h"
#include "Components/SceneComponent.h"
#include "Villaggio.generated.h"

UCLASS()
class TD_DRUNKENSAILOR_API AVillaggio : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AVillaggio();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UPROPERTY(EditDefaultsOnly)
		USceneComponent* Mesh;
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
		float HP = 0;

	UFUNCTION(BlueprintImplementableEvent)
		void End();
	UFUNCTION()
		void OnOverlapBegin(AActor* OverlappedActor, AActor* OtherActor);
};
