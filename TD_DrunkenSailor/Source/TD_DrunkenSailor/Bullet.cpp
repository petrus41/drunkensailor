// Fill out your copyright notice in the Description page of Project Settings.


#include "Bullet.h"

// Sets default values
ABullet::ABullet()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	SetRootComponent(Mesh);
}

// Called when the game starts or when spawned
void ABullet::BeginPlay()
{
	Super::BeginPlay();
	Start = GetActorLocation();
	End = Target->GetActorLocation();
	Speed = -0.5 * Gravity * TravelTime + ((End.Z - Start.Z) / TravelTime);
}


// Called every frame
void ABullet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
		
	if (Timer < TravelTime)
	{
		Timer += DeltaTime;
		if (Target)
		{
			if (BParable)
			{
				float G = Gravity * 0.5 * FMath::Square(Timer);
				float V = Speed * Timer;
				float Z = Start.Z;
				SetActorLocation(FVector(FMath::Lerp(Start.X, Target->GetActorLocation().X, Timer / TravelTime), FMath::Lerp(Start.Y, Target->GetActorLocation().Y, Timer / TravelTime),G+V+Z));
				//UE_LOG(LogTemp, Error, TEXT("%f"), V);
			}
			else
			{
				SetActorLocation(FMath::Lerp(Start, Target->GetActorLocation(), Timer/TravelTime));
			}
		}
	}
	else 
	{
		AOECollision();
	}
}

