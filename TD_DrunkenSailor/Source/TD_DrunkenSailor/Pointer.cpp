// Fill out your copyright notice in the Description page of Project Settings.


#include "Pointer.h"

// Sets default values
APointer::APointer()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SetRootComponent(Root);
	Decal= CreateDefaultSubobject<UDecalComponent>(TEXT("Decal"));
	Decal->SetupAttachment(Root);
}

// Called when the game starts or when spawned
void APointer::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APointer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (BSet)
	{
		TArray< TEnumAsByte< EObjectTypeQuery > > ObjectTypes;
		ObjectTypes.Add(EObjectTypeQuery::ObjectTypeQuery1);
		FHitResult Hit;
		GetWorld()->GetFirstPlayerController()->GetHitResultUnderCursorForObjects(ObjectTypes, true, Hit);
		SetActorLocation(Hit.Location);
	}

}

