// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PawnCamera.h"
#include "GameFramework/PlayerController.h"
#include "DrukenSailorPlayerController.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FStart);

UCLASS()
class TD_DRUNKENSAILOR_API ADrukenSailorPlayerController : public APlayerController
{
	GENERATED_BODY()
protected:

	virtual void SetupInputComponent() override;
public:
	virtual void BeginPlay() override;
	ADrukenSailorPlayerController();

	UPROPERTY(BlueprintCallable, BlueprintAssignable)
		FStart Begin;
	UPROPERTY(BlueprintCallable, BlueprintAssignable)
		FStart Shoot;

	void Start();
	void Rotate(float Direction);
	void Forward(float Direction);
	void Left(float Direction);
	void Up(float Direction);
	void Fire();
	void Exit();

	APawnCamera* Pawn = nullptr;
	
};
