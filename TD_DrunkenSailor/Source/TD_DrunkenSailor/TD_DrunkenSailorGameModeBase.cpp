// Copyright Epic Games, Inc. All Rights Reserved.


#include "TD_DrunkenSailorGameModeBase.h"
#include "DrukenSailorPlayerController.h"
#include "Kismet/GameplayStatics.h"

ATD_DrunkenSailorGameModeBase::ATD_DrunkenSailorGameModeBase()
{
	// use our custom PlayerController class
	PlayerControllerClass = ADrukenSailorPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character

}

void ATD_DrunkenSailorGameModeBase::BeginPlay()
{
	Super::BeginPlay();
	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlacer::StaticClass(), FoundActors);
	for (AActor* Placer : FoundActors)
	{
		APlacer* Temp = Cast<APlacer>(Placer);
		if (!Temp)
		{
			UE_LOG(LogTemp, Error, TEXT("no placer in the world"));
		}
		else
		{
			AllPlacer.AddUnique(Temp);
		}
	}
}

