// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Placer.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FCliked);

UCLASS()
class TD_DRUNKENSAILOR_API APlacer : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APlacer();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	FCliked Cliked;

	UFUNCTION()
		void OnSelected(AActor* TouchedActor, FKey ButtonPressed);

};
