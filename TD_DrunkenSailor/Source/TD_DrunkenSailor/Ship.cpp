// Fill out your copyright notice in the Description page of Project Settings.


#include "Ship.h"
#include "Tower.h"
#include "Villaggio.h"

// Sets default values
AShip::AShip(const FObjectInitializer& OI) :Super(OI)
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MovementComponent= CreateDefaultSubobject<UFloatingPawnMovement>(TEXT("MovementComponent"));
	Root = OI.CreateDefaultSubobject<USceneComponent>(this, FName("Root"));
	SetRootComponent(Root);
	Mesh = OI.CreateDefaultSubobject<UStaticMeshComponent>(this, FName("Ship"));
	Mesh->SetupAttachment(Root);
	Capsule= OI.CreateDefaultSubobject<UCapsuleComponent>(this, FName("AOE"));
	Capsule->SetupAttachment(Root);
}

// Called when the game starts or when spawned
void AShip::BeginPlay()
{
	Super::BeginPlay();

	Capsule->SetCapsuleHalfHeight(Area * 5);
	Capsule->SetCapsuleRadius(Area);
	MaxHP = HP;
	Mesh->OnComponentBeginOverlap.AddDynamic(this, &AShip::OnOverlapBegin);
	Capsule->OnComponentBeginOverlap.AddDynamic(this, &AShip::Overlapping);
	Capsule->OnComponentEndOverlap.AddDynamic(this, &AShip::EndOverlap);
	if (MovementComponent)
	{
		MovementComponent->MaxSpeed = Speed;
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("NO Ship"));
	}
	GameMode = Cast<ATD_DrunkenSailorGameModeBase>(GetWorld()->GetAuthGameMode());
}

// Called every frame
void AShip::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (Targets.Num() > 0)
	{
		Timer += DeltaTime;
		if (Timer > 1 / FireRate)
		{
			Spawn();
			//UE_LOG(LogTemp, Error, TEXT("At Leat One Target"));
			Timer -= 1 / FireRate;
		}
	}
}


// Called to bind functionality to input
void AShip::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}


void AShip::Spawn() 
{
	FTransform MyTransform(FRotator(0, 0, 0), FVector(GetActorLocation().X, GetActorLocation().Y, GetActorLocation().Z ), FVector(0.5, 0.5, 0.5));
	NewBullet = GetWorld()->SpawnActorDeferred<ABullet>(Bullet, MyTransform);
	NewBullet->Target = Targets[0];
	NewBullet->Damage = Damage;
	NewBullet->Generetor = EGeneretor::Ship;
	NewBullet->TravelTime = TravelTime;
	NewBullet->BParable = BParable;
	NewBullet->Start = FVector(GetActorLocation().X, GetActorLocation().Y, GetActorLocation().Z + 100);
	NewBullet->FinishSpawning(MyTransform);
}

void AShip::Overlapping(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	ATower* Tower = Cast<ATower>(OtherActor);
	UStaticMeshComponent* StaticMesh = Cast<UStaticMeshComponent>(OtherComp);
	if (Tower&&StaticMesh)
	{
		UE_LOG(LogTemp, Error, TEXT("Overlap"));
		Targets.AddUnique(Tower);
	}
}

void AShip::EndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	ATower* Tower = Cast<ATower>(OtherActor);
	UStaticMeshComponent* StaticMesh = Cast<UStaticMeshComponent>(OtherComp);
	if (Tower&&StaticMesh)
	{
		Targets.Remove(Tower);
	}
}

void AShip::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Hitter = Cast<ABullet>(OtherActor);
	AVillaggio* End = Cast<AVillaggio>(OtherActor);
	if (Hitter) 
	{
		if (Hitter->Generetor == EGeneretor::Tower)
		{
			Hitted.Broadcast();
			if (Hitter->Damage - Armor > 0)
			{
				HP -= Hitter->Damage - Armor;
			}
			else
			{
				HP -= 1;
			}
			if (HP <= 0)
			{
				Eliminated.Broadcast();
				GameMode->Gold += Award;
				GameMode->WidgetUpdate();
				Destroy();
			}
			if (Hitter->Slow > rallentamento)
			{
				rallentamento = Hitter->Slow;
			}
			MovementComponent->MaxSpeed = Speed * (1-rallentamento);
			UE_LOG(LogTemp, Error, TEXT("%f"), MovementComponent->MaxSpeed);
		}
	}
	else if (End)
	{
		VillageHitted.Broadcast();
		Destroy();
	}
}

void AShip::Colpita(float danni,float slow)
{
	if (danni - Armor > 0)
	{
		HP -= danni-Armor;
	}
	else
	{
		HP -= 1;
	}
	Hitted.Broadcast();
	if (slow > rallentamento)
	{
		rallentamento = slow;
	}
	MovementComponent->MaxSpeed = Speed * 1-rallentamento;
	if (HP <= 0)
	{
		Eliminated.Broadcast();
		GameMode->Gold += Award;
		GameMode->WidgetUpdate();
		Destroy();
	}
}
