// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Ship.h"
#include "GameFramework/Actor.h"
#include "WaveSpawner.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FEnd);

UCLASS()
class TD_DRUNKENSAILOR_API AWaveSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWaveSpawner();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ActorSpawning", meta = (ExposeOnSpawn = "true"))
		TSubclassOf<AShip> Ship;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ExposeOnSpawn = "true"))
		float TimeBeforeSpawn = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ExposeOnSpawn = "true"))
		float SpawnRate = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ExposeOnSpawn = "true"))
		int WavePopulation = 0;

	UPROPERTY(BlueprintCallable, BlueprintAssignable)
		FEnd End;
	UPROPERTY(BlueprintReadWrite)
		AShip* NewShip = nullptr;
	FTimerHandle SpawnTimerHandle;
	int SpawnerIteretor = 0;
	int DeathIteretor = 0;


	UFUNCTION(BlueprintImplementableEvent)
		void Blueprint();

	UFUNCTION()
		void Check();
	UFUNCTION()
		void Spawn();
};
