// Fill out your copyright notice in the Description page of Project Settings.


#include "Tower.h"
#include "Ship.h"
#include "Kismet/KismetMathLibrary.h"

#define OUT
// Sets default values
ATower::ATower(const FObjectInitializer& OI) :Super(OI)
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Root = OI.CreateDefaultSubobject<USceneComponent>(this, FName("Root"));
	SetRootComponent(Root);
	Tower = OI.CreateDefaultSubobject<UStaticMeshComponent>(this, FName("Mesh"));
	Tower->SetupAttachment(Root);
	Capsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Capsule"));
	Capsule->SetupAttachment(Root);
	Decal = CreateDefaultSubobject<UDecalComponent>(TEXT("Decal"));
	Decal->SetupAttachment(Root);
}

// Called when the game starts or when spawned
void ATower::BeginPlay()
{
	Super::BeginPlay();
	Decal->SetWorldScale3D(FVector(1.f, Statistiche[Livello].Raggio / 50, Statistiche[Livello].Raggio / 50));
	Capsule->SetCapsuleRadius(Statistiche[Livello].Raggio);
	Capsule->SetCapsuleHalfHeight(Statistiche[Livello].Raggio * 5);
	HP=Statistiche[Livello].MAXHP;
	Capsule->OnComponentBeginOverlap.AddDynamic(this, &ATower::Overlapping);
	Capsule->OnComponentEndOverlap.AddDynamic(this, &ATower::EndOverlap);
	OnClicked.AddUniqueDynamic(this, &ATower::OnSelected);
	Tower->OnComponentBeginOverlap.AddDynamic(this, &ATower::OnOverlapBegin);
	GameMode = Cast<ATD_DrunkenSailorGameModeBase>(GetWorld()->GetAuthGameMode());
	if (!GameMode)
	{
		UE_LOG(LogTemp, Error, TEXT("NO GameMode"));
	}
}

// Called every frame
void ATower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (BSet)
	{
		TArray< TEnumAsByte< EObjectTypeQuery > > ObjectTypes;
		ObjectTypes.Add(EObjectTypeQuery::ObjectTypeQuery1);
		FHitResult Hit;
		GetWorld()->GetFirstPlayerController()->GetHitResultUnderCursorForObjects(ObjectTypes, true, Hit);
		APlacer* Placer = Cast<APlacer>(Hit.Actor);
		if (Placer)
		{
			SetActorLocation(Hit.Actor->GetActorLocation());
			Placer->Cliked.AddUniqueDynamic(this, &ATower::Place);
		}
		else
		{
			SetActorLocation(Hit.Location);
		}
	}
	else if(!BRepairing&&!BSet)
	{
		if (Targets.Num() > 0)
		{
			SetActorRotation(FRotator(GetActorRotation().Pitch,UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), Targets[0]->GetActorLocation()).Yaw, GetActorRotation().Roll));
			Timer += DeltaTime;
			if (Timer > 1 / Statistiche[Livello].FireRate)
			{
				Fire();
				//UE_LOG(LogTemp, Error, TEXT("At Leat One Target"));
				Timer -= 1 / Statistiche[Livello].FireRate;
			}
		}
	}
	else if (BRepairing)
	{
		RepairTimer += DeltaTime;
		HP+=((Statistiche[Livello].MAXHP * MissingHealthPrecent) / (MissingHealthPrecent * 10))*DeltaTime;
		HealthPrecent = HP / Statistiche[Livello].MAXHP;
		UpdateWidget();
		if (RepairTimer >= MissingHealthPrecent * 10)
		{
			BRepairing = false;
			RepairTimer = 0;
		}
	}
}

void ATower::Overlapping(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AShip* Ship = Cast<AShip>(OtherActor);
	UStaticMeshComponent* StaticMesh = Cast<UStaticMeshComponent>(OtherComp);
	if (Ship&&StaticMesh)
	{
		Targets.AddUnique(OtherActor);
	}
}

void ATower::EndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	AShip* Ship = Cast<AShip>(OtherActor);
	UStaticMeshComponent* StaticMesh = Cast<UStaticMeshComponent>(OtherComp);
	if (Ship && StaticMesh)
	{
		Targets.Remove(OtherActor);
	}
}

void ATower::OnSelected(AActor* TouchedActor, FKey ButtonPressed)
{
	Place();
}


void ATower::Fire()
{
	FTransform MyTransform(FRotator(0, 0, 0), FVector(GetActorLocation().X, GetActorLocation().Y, GetActorLocation().Z + 100), FVector(0.5, 0.5, 0.5));
	NewBullet = GetWorld()->SpawnActorDeferred<ABullet>(Bullet, MyTransform);
	NewBullet->Target = Targets[0];
	NewBullet->Damage = Statistiche[Livello].Damage;
	NewBullet->Generetor = Generetor;
	NewBullet->BParable = BParable;
	NewBullet->AOERange = AOE;
	NewBullet->Slow = Statistiche[Livello].Slow;
	NewBullet->TravelTime = TravelTime;
	NewBullet->Start = FVector(GetActorLocation().X, GetActorLocation().Y, GetActorLocation().Z + 100);
	NewBullet->FinishSpawning(MyTransform);
	Anim();
}

void ATower::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	ABullet* Proiettile = Cast<ABullet>(OtherActor);
	if (Proiettile)
	{
			UE_LOG(LogTemp, Error, TEXT("Overlap"));
		if (Proiettile->Generetor == EGeneretor::Ship)
		{
			HP -= Proiettile->Damage;
			HealthPrecent = HP / Statistiche[Livello].MAXHP;
			Hitted.Broadcast();
			if (HP <= 0)
			{
				GameMode->AllPlacer.AddUnique(Position);
				Destroy();
			}
		}
	}
}


void ATower::Place()
{
	if (BSet)
	{
		TArray< TEnumAsByte< EObjectTypeQuery > > ObjectTypes;
		ObjectTypes.Add(EObjectTypeQuery::ObjectTypeQuery1);
		FHitResult Hit;
		GetWorld()->GetFirstPlayerController()->GetHitResultUnderCursorForObjects(ObjectTypes, true, Hit);
		Position = Cast<APlacer>(Hit.Actor);
		if (Position && GameMode->AllPlacer.Contains(Position))
		{
			BSet = false;
			UpdateCollision();
			Decal->SetVisibility(false);
			GameMode->AllPlacer.Remove(Position);
		}
	}
}

void ATower::Repair()
{
	float MissingHealth = Statistiche[Livello].MAXHP - HP;
	if (MissingHealth > 0)
	{
		MissingHealthPrecent =  MissingHealth/Statistiche[Livello].MAXHP ;
		int RepairCost = GameMode->Costi.Cannone * MissingHealthPrecent;
		if (RepairCost < 1)
		{
			RepairCost = 1;
		}
		if (GameMode->Gold >= RepairCost/2)
		{
			GameMode->Gold-=RepairCost/2;
			GameMode->WidgetUpdate();
			BRepairing = true;
		}
	}
}

void ATower::UpGrade()
{
	if(Livello<3)
	{
		if (GameMode->Gold >= Costi_Upgrade[Livello])
		{
			GameMode->Gold -= Costi_Upgrade[Livello];
			GameMode->WidgetUpdate();
			Livello++;
			Decal->SetWorldScale3D(FVector(1.f, Statistiche[Livello].Raggio / 50, Statistiche[Livello].Raggio / 50));
			Capsule->SetCapsuleRadius(Statistiche[Livello].Raggio);
			Capsule->SetCapsuleHalfHeight(Statistiche[Livello].Raggio * 5);
			HP = Statistiche[Livello].MAXHP;
			UpdateWidget();
		}
	}
}